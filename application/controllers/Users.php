<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller
{
  protected function image()
  {

  }
    public function index()
    {
      $donne =  $this->accmo->get()->result();

      $donnee=array();
      $i=0;
      foreach ($donne as $row){
          $data[$i]["type"]=$row->type;
          $data[$i]["image"]=$row->image;
          $i++;
      }
      $donnee=$data;
//        die(var_dump($data));
        $this->load->view('common/header');
        $this->load->view('acceuil', compact("donnee"));
        $this->load->view('common/footer');
    }
    public function album()
    {
      $donne =  $this->accmo->get()->result();

      $donnee=array();
      $i=0;
      foreach ($donne as $row){
          $data[$i]["type"]=$row->type;
          $data[$i]["image"]=$row->image;
          $data[$i]["id"]=$row->id;
          $data[$i]["votes"]=$this->accmo->get_vote($row->id);
          $i++;
      }
      $donnee=$data;
//        die(var_dump($data));
        $this->load->view('common/header');
        $this->load->view('album', compact("donnee"));
        $this->load->view('common/footer');
    }

    public function history()
    {
      $this->load->view('common/header');
      $this->load->view('histoire');
      $this->load->view('common/footer');
    }

    public function save_vote(){
        $ip=$this->uri->segment(3);
        $id_image=$this->uri->segment(4);
       // die(var_dump(array($id_candidate,$ip)));
        $ip=1100;
        if ($this->accmo->check_ip($ip)==false)
        {
            $this->accmo->save_vote($ip,$id_image);
            redirect(base_url()."users/album");
        }
        redirect(base_url()."users/album");


    }


}
