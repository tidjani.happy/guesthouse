    <?php
defined('BASEPATH') OR exit('No direct script access allowed');

class acceuil_Model extends CI_Model {

	public function get()
	{
		$all = $this->db->select("*")
				->from("img_acceuil")
				->get();

		return $all;
	}
    public function get_vote($id_image)
    {
        $all = $this->db->select("id_votant")
            ->from("info_votant")
            ->where("id_photo",$id_image)
            ->get()
            ->num_rows();

        return $all;
    }

    public function check_ip($ip)
    {
        $check = $this->db->select("*")
            ->from("info_votant")
            ->where("ip_votant",$ip)
            ->get()
            ->num_rows();
        if ($check>0){
            return true;
        }else{
            return false;
        }
    }

    public function save_vote($ip,$id_image)
    {
        $this->db->insert("info_votant",array("id_photo"=>$id_image,"date_vote"=>date('Y-m-d H:i:s'),"ip_votant"=>$ip));

    }
}
