<!-- Nav -->
<nav id="nav">
		<ul class="links">
			<li class="active"><a href="<?php echo base_url()?>index.php/Users">Accueil</a></li>
			<li><a href="<?php echo base_url()?>index.php/Users/album">Album</a></li>
			<li><a href="<?php echo base_url()?>index.php/Users/history">A propos</a></li>
		</ul>
		<ul class="icons">
			<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
			<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
			<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
			<li><a href="#" class="icon fa-google-plus"><span class="label">Google plus</span></a></li>
		</ul>
	</nav>
									<!-- Main -->
										<div id="main">

											<!-- Featured Post -->
												<article class="post featured">
													<header class="major">
														<span class="date">
					                    <?php
					                    $datetime = date("d-m-Y || H:i:s");
					                    echo $datetime;?>
														</span>
														<h1> SOYEZ LES BIENVENUES </h1>
														<p>Chambre de luxe - Restauration -Vue panoramique sur l'Etoile rouge -Wifi -Canal+</p>
													</header>
													<a href="<?php echo base_url()?>index.php/Users/album" class="image main">
														<div class="row">
															<div class="col-7"><img src="<?php echo base_url()?>assets/images/guestjour/gj0.jpg" alt="" style=" height: 478px"/></div>
															<div class="col-5">
																<div class ="row vertical-align">
																	<div class="align-self-start"><img src="<?php echo base_url()?>assets/images/guestjour/gj11.jpg" alt="" /></div>
																	<div class="align-self-end"><img src="<?php echo base_url()?>assets/images/guestjour/gj25.jpg" alt="" /></div>
																</div>
															</div>
														</div>

														</a>
													<ul class="actions special">
														<li><a href="<?php echo base_url()?>index.php/Users/history" class="button large">L'histoire</a></li>
													</ul>

													<p>La GuestHouse Ifedu ouvre ses portes avec tes appartements luxieux et chaleureux dans un cadre paisible, acceuillant, sécurisé qui vous laisseront de très beaux souvenirs. Vous etes là pour affaires, vacances, ou en famille plus de soucis à vous faire,
													la guesthouse ifedu vous offre tout le confort dont vous avez besoin pour que ce séjour soit agréable.</p>
												</article>
										</div>

	
