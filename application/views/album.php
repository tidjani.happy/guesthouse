<!-- Nav -->
	<nav id="nav">
		<ul class="links">
			<li ><a href="<?php echo base_url()?>index.php/Users">Accueil</a></li>
			<li class="active"><a href="<?php echo base_url()?>index.php/Users/album">Album</a></li>
			<li><a href="<?php echo base_url()?>index.php/Users/history">A propos</a></li>
		</ul>
		<ul class="icons">
			<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
			<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
			<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
			<li><a href="#" class="icon fa-google-plus"><span class="label">Google plus</span></a></li>
		</ul>
	</nav>
									<!-- Main -->
										<div id="main">

											<!-- Featured Post -->
												<article class="post featured">
													<header class="major">
														<span class="date">
					                    <?php
					                    $datetime = date("d-m-Y || H:i:s");
					                    echo $datetime;?>
														</span>
														<h1>Voyez par vous même</h1>
														<p>La GuestHouse Ifedu ouvre ses portes avec tes appartements luxieux et chaleureux qui vous laisseront de très beaux souvenirs.</p>
													</header>
													<a href="#" class="image main"><img src="<?php echo base_url()?>assets/images/guestjour/gj25.jpg" alt="" /></a>
													<ul class="actions special">
														<li><a href="<?php echo base_url()?>index.php/Users/history" class="button large">L'histoire</a></li>
													</ul>
												</article>

											<!-- Posts -->
												<section class="posts">

													<?php
													        foreach ($donnee as $row):
																				   ?>
													                <article>

													                    <a  class="image fit"><img src="<?=image_url($row['image']);?>" alt="" /></a>
													                    <ul class="actions">
													                    	<h3><?php echo $row['votes'] ; if ($row['votes']>=2) echo "<label>Votes</label>"; else echo "<label>Vote</label>" ?>  </h3>
													                        <li><a id="<?php echo $row['id'] ;?> " style="background-color:#cd9459; border-radius: 100px" class="val button special icon fa-heart-o">J'AIME</a></li><br>
													                        <h2> <span></span></h2>
													                </article>
													            <?php

													            endforeach; ?>
												</section>

											<!-- Footer -->
												<footer>
													<div class="pagination">
														<!--<a href="#" class="previous">Prev</a>-->
														<a href="#" class="page active">1</a>
														<a href="#" class="page">2</a>
														<a href="#" class="page">3</a>
														<span class="extra">&hellip;</span>
														<a href="#" class="page">8</a>
														<a href="#" class="page">9</a>
														<a href="#" class="page">10</a>
														<a href="#" class="next">Next</a>
													</div>
												</footer>

										</div>
