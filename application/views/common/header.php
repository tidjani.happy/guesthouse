<!DOCTYPE HTML>
<!--
	Massively by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Guesthouse IFEDU</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/main.css" />
		<noscript><link rel="stylesheet" href="<?php echo base_url()?>assets/css/noscript.css" /></noscript>
	</head>
	<body class="is-preload">

		<!-- Wrapper -->
			<div id="wrapper">
        <!-- Intro -->
        					<div id="intro">
        						<h1>This is<br />
        						IFEDU GuestHouse</h1>
        						<ul class="actions">
        							<li><a href="#header" class="button icon solo fa-arrow-down scrolly">Continue</a></li>
        						</ul>
        					</div>
				<!-- Header -->
					<header id="header">
						<a href="<?php echo base_url()?>index.php/Users" class="logo">GuestHouse</a>
					</header>
					
