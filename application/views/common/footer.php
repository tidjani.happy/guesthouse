<!-- Footer -->
  <footer id="footer">
    <section>
      <form method="post" action="#">
        <div class="fields">
          <div class="field">
            <label for="name">Nom</label>
            <input type="text" name="name" id="name" />
          </div>
          <div class="field">
            <label for="email">Email</label>
            <input type="text" name="email" id="email" />
          </div>
          <div class="field">
            <label for="message">Message</label>
            <textarea name="message" id="message" rows="3"></textarea>
          </div>
        </div>
        <ul class="actions">
          <li><input type="submit" value="Send Message" /></li>
        </ul>
      </form>
    </section>
    <section class="split contact">
      <section class="alt">
        <h3>Adresse</h3>
        <p>C/1127, Sainte rita ,ruelle en face du bureau de poste, <b>Immeuble de L'Espace Ifedu.</b></p>
      </section>
      <section>
        <h3>Phone</h3>
        <p>
          <a>(+229) 61.32.16.08</a>
        </br>
          <a>(+229) 97.72.10.57</a>
           </br>
          <a>(+229) 96.68.07.28</a>
        </p>
      </section>
      <section>
        <h3>Email</h3>
        <p><a href="#">info@ifeduguesthouse.com</a></p>
      </section>
      <section>
        <h3>Social</h3>
        <ul class="icons alt">
          <li><a href="#" class="icon alt fa-twitter"><span class="label">Twitter</span></a></li>
          <li><a href="#" class="icon alt fa-facebook"><span class="label">Facebook</span></a></li>
          <li><a href="#" class="icon alt fa-instagram"><span class="label">Instagram</span></a></li>
          <li><a href="#" class="icon alt fa-google-plus"><span class="label">Google plus</span></a></li>
        </ul>
      </section>
    </section>
  </footer>

<!-- Copyright -->
  <div id="copyright">
    <ul><li>&copy; Ifedu 2018</li><li>Conception web : <a href="https://www.facebook.com/HappyTechLab/" target="blank">HappyLab</a></li></ul>
  </div>

</div>

<!-- Scripts -->
<script src="<?php echo base_url()?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.scrollex.min.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.scrolly.min.js"></script>
<script src="<?php echo base_url()?>assets/js/browser.min.js"></script>
<script src="<?php echo base_url()?>assets/js/breakpoints.min.js"></script>
<script src="<?php echo base_url()?>assets/js/util.js"></script>
<script src="<?php echo base_url()?>assets/js/main.js"></script>
<script type="text/javascript">
    var base_url="<?php echo base_url()?>";
    var nbclic1=0;
    var nbclicmax1=1;
    $(".val").click(function () {
        nbclic1++
        var id = $(this).attr("id");
        console.log(id);
//        adresse = java.net.InetAddress.getLocalHost();
//
//        ip = adresse.getHostAddress();

        var ipp=0;
        $.getJSON("https://api.ipify.org?format=jsonp&callback=?",
            function(json) {
            json.mac
                window.location=base_url+"users/save_vote/"+json.ip+"/"+id;
                console.log("Passage à save_vote");
            }
        );

    });
</script>
</body>
</html>
