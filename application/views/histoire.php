<!-- Nav -->
	<nav id="nav">
		<ul class="links">
			<li><a href="<?php echo base_url()?>index.php/Users">Accueil</a></li>
			<li><a href="<?php echo base_url()?>index.php/Users/album">Album</a></li>
			<li class="active"><a href="<?php echo base_url()?>index.php/Users/history">A propos</a></li>
		</ul>
		<ul class="icons">
			<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
			<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
			<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
			<li><a href="#" class="icon fa-google-plus"><span class="label">Google plus</span></a></li>
		</ul>
	</nav>
				<!-- Main -->
					<div id="main">

						<!-- Post -->
							<section class="post">
								<header class="major">
									<span class="date">
					                    <?php
					                    $datetime = date("d-m-Y || H:i:s");
					                    echo $datetime;?>
														</span>
									<h1>Renseignez vous sur le<br />
									Guesthouse </h1>
									<p>Le <b>Guesthouse Ifedu</b> est un lieu qui vous fera entourer votre ou vos nuits d'un doux parfum d'exotisme mêlé au charme de l'Urbanisme africain.</p>
								</header>
								<div class="image main"><img src="<?php echo base_url()?>assets/images/pic01.jpg" alt="" /></div>
								<p>Le Batiment d'un seul tenant qui abritte le Guesthouse est en effet un grand immeuble nommé <b>L'Espace Ifedu</b>. Si vous recherchez la paix et le calme durant votre séjour, vous devriez trouver votre bonheur dans le Guesthouse de l'Espace Ifedu. Le Guesthouse de l'Espace Ifedu est repérable de la route car elle est en plein coeur de Cotonou.</p>
								<p>Chacune des trois chambres est dotée d'un lit de trois (3) places, d'une douche équipée de chauffe-eau et biensûr de tout ce qui est nécessaire dans une douche. Le salon commun est pourvu d'une télévision et d'une somptueuse salle à manger. Les chambres et le salon sont tous équipés d'air conditionnés. <b>The Guesthouse Ifedu </b>possède des balcons et offre une vue panoramique sur la ville de cotonou.</p>
							</section>

					</div>
